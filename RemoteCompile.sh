!/bin/bash

# This script will automatically compile and copy binary file to Raspberry Pi
# IMPORTANT: Check IP address

cmake -DCMAKE_TOOLCHAIN_FILE=Toolchain-ARM.cmake -DROBOT=VECI -H. -BarmBuild_miokvrag
make -j3 --directory armBuild_miokvrag
mv bin/eurobot2k15 bin/LD_eurobot2k15
scp bin/LD_eurobot2k15 pi@192.168.0.150:~/Eurobot-2015/eurobot2k15/bin
