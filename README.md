# Memristor Eurobot Platform 
This project is started by Pandurov & Tumbas. Since 2016 we want to use this project as a standard platform for developing robots for Eurobot competition. 
Our goal is to continue developing this platform using existent technologies and routines.

## Host dependencies
- Git client
- CMake
- RSync  

To install all dependencies on Debian derivates run `sudo apt-get install git cmake rsync`

## Configure platform
### Standard configuration
This repositorium already contains intergrated libraries so our advice is to use them. Libraries are compiled and ready to use out of the box.  

But you still need to follow these steps:  

1. [Configure Raspberry Pi](docs/rpiConfig.md)
2. Copy directories, `/usr` & `/lib` from Rasspberry Pi to `[your eurobot directory]/tools/rootfs`  
	```
	rsync -rl --delete-after --safe-links pi@192.168.1.247:/{lib,usr} [your eurobot directory]/tools/rootfs  
	```

### Manually configure platform
If you want to dig and install new version of libraries follow steps bellow:  

1. [Configure Raspberry Pi](docs/rpiConfig.md)
2. [ToolChain](docs/toolchain.md)
3. [Boost Library](docs/boost.md)
4. [Google's v8 Engine](docs/v8.md)


## Build platform  

### Auto build
- Run ```./Compile.sh```

### Manual build
- Change directory to [your eurobot directory] and run:  
```
	cmake -DCMAKE_TOOLCHAIN_FILE=Toolchain-ARM.cmake -DROBOT=VECI -H. -BarmBuild_miokvrag
```

- Change directory to armBuild_miokvrag `cd armBuild_miokvrag`
```
	make -j3
```
- Copy binary to Raspberry Pi

## Run eurobot
- Format `./eurobot2k15 strategy_path.xml scheduler_path.js strategies_directory`
	- Eg. `sudo ./eurobot2k15 strategies/standard_strategy.xml strategies/standard_scheduler.js strategies/tasks`

	
