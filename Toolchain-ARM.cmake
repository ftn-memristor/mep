SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_C_COMPILER ${PROJECT_SOURCE_DIR}/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER ${PROJECT_SOURCE_DIR}/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-g++)

#SET(CMAKE_C_COMPILER /usr/bin/arm-linux-gnueabihf-gcc-4.8 )
#SET(CMAKE_CXX_COMPILER /usr/bin/arm-linux-gnueabihf-g++-4.8 )

SET(CMAKE_FIND_ROOT_PATH ${PROJECT_SOURCE_DIR}/tools/rootfs)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(CMAKE_CROSSCOMPILING 1)

