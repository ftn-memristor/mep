#ifndef _PNEUMATIKA
#define _PNEUMATIKA

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <stdio.h>
#include <stdlib.h>

#include "utils/modbus/ModbusClientSwitzerland.h"
#include "utils/modbus/ModbusMaster.h"

using namespace modbus;
using modbus::ModbusMaster;
using modbus::ModbusClientSW;

namespace pneumatika {
	class Pneumatika {
		private:
			unsigned char slave_address;
			unsigned short address;
			unsigned short position;
			boost::mutex *s_mutex;
			ModbusClientSW* modbus;
		public:
			Pneumatika();
			Pneumatika(unsigned char, unsigned short, unsigned short);
			~Pneumatika();
			bool setPosition(unsigned char);
			bool readPosition();
			unsigned char getPneumatikaPosition();
			void setPneumatikaPosition(unsigned short);
			void setPneumatikaAddress(unsigned short);
			void setPneumatikaSlaveAddress(unsigned char);
	};
}

#endif /* ifndef _PNEUMATIKA */
