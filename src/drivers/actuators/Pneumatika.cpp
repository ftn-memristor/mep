#include "Pneumatika.h"

namespace pneumatika {

	Pneumatika::Pneumatika():s_mutex(new boost::mutex()) {
		this->slave_address = 'a';
		this->address = 'a';
		this->position = 0;
		modbus = ModbusClientSW::getModbusClientInstance();
	}

	Pneumatika::Pneumatika(unsigned char slave_address = 'a', unsigned short address = 'a', unsigned short position = 0):s_mutex(new boost::mutex()) {
		this->slave_address = slave_address;
		this->address = address;
		modbus = ModbusClientSW::getModbusClientInstance();
	}

	Pneumatika::~Pneumatika() {
		delete s_mutex;
	}

	bool Pneumatika::setPosition(unsigned char position) {
		bool success;
		if ((position==1)||(!position)) {
			success = modbus->setCoil(this->slave_address, this->address, position, false);
		}
		else {
			printf("ERROR: POSITION OUT OF VALUE");
			return false;
		}
		if (success)
			this->position = position;

		return success;

	}
	bool Pneumatika::readPosition() {
		return position;
	}

	unsigned char Pneumatika::getPneumatikaPosition() {
		return position;
	}
	void Pneumatika::setPneumatikaPosition(unsigned short position) {
		this->position = position;
	}
	void Pneumatika::setPneumatikaAddress(unsigned short address) {
		this->address = address;
	}
	void Pneumatika::setPneumatikaSlaveAddress(unsigned char slave_address) {
		this->slave_address = slave_address;
	}
}
