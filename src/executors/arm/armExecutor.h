#ifndef _ARM_EXECUTOR_H
#define _ARM_EXECUTOR_H 

#include "executors/ExecutorCommon.h"

namespace executor {

class ArmExecutor: public ExecutorCommon {
public:
	ArmExecutor():ExecutorCommon(this->NAME){}
	static string NAME;
	void mapping();
	void suscribe();
private:
	bool GetObjectL0Function();
	bool GetObjectL0HoldFunction();
	bool GetObjectL1Function();
	bool GetObjectL2Function();
	bool ReleaseObjectFunction();
	bool TakeArmOutFunction();

	bool loaded=false;

	ServoDriver Maxon;
	ServoDriver AX1;
	ServoDriver AX2;
	ServoDriver Kinez;
};
}

#endif /* ifndef _ARM_EXECUTOR_H */
