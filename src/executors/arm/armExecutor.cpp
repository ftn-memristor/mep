#include "armExecutor.h"

namespace executor {

string ArmExecutor::NAME = "ArmExecutor";

void ArmExecutor::suscribe(){
	this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&ArmExecutor::processActuatorCommand));
}

void ArmExecutor::mapping(){
	Maxon.setServoSlaveAddress(2);
	Maxon.setServoPositionAddress(2);
	Maxon.setServoSpeedAddress(1);

	AX1.setServoSlaveAddress(1);
	AX1.setServoPositionAddress(2);

	AX2.setServoSlaveAddress(1);
	AX2.setServoPositionAddress(3);

	Kinez.setServoSlaveAddress(1);
	Kinez.setServoPositionAddress(6);

	reload(&value,executorName);

	Maxon.setServoSpeed(30);

	/*
	Kinez.rotateToPosition(value.ArmConfigs.KinezClosed); //800
	AX1.rotateToPosition(value.ArmConfigs.AX1Closed); //800
	AX2.rotateToPosition(value.ArmConfigs.AX2Closed); //480
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	*/
}

bool ArmExecutor::TakeArmOutFunction(){
	bool success;
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position1);//790
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Position1);//510
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));//30
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position2);//740
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Position2);//540
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));//50
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position3);//700
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Position3);//560
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));//40
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position4);//600
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Position4);//100
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));//110
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position5); //512
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Position5);//210
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(750));//110
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front); //512
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);//210
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));//350
	return true;
}

bool ArmExecutor::GetObjectL1Function(){
	bool success;
	debug("MAXON TEST");
//	if (loaded)
//		return false;
	/*TODO - Izvlacenje ruke */
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezOpened);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonLevel1);
	if (!success) {
		debug("Maxon failed to reach level1");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(1200));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezClosed);
	if (!success) {
		debug("Kinez se nije zatvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(1200));
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position4);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	loaded = true;
	return true;
}

bool ArmExecutor::GetObjectL2Function(){
	bool success;
	if (loaded)
		return false;
	/*TODO - Izvlacenje ruke */
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezOpened);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonLevel2);
	if (!success) {
		debug("Maxon failed to reach level1");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(700));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezClosed);
	if (!success) {
		debug("Kinez se nije zatvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	/*TODO - Sklanjanje ruke */
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(700));
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position4);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	loaded = true;
	return true;
}
bool ArmExecutor::GetObjectL0HoldFunction(){
	bool success;
	if (loaded)
		return false;
	/*TODO - Izvlacenje ruke */
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezOpenedHold);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonLevel0);
	if (!success) {
		debug("Maxon failed to reach level1");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezClosedHold);
	if (!success) {
		debug("Kinez se nije zatvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	loaded = true;
	return true;
}

bool ArmExecutor::GetObjectL0Function(){
	bool success;
	if (loaded)
		return false;
	/*TODO - Izvlacenje ruke */
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezOpened);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonLevel0);
	if (!success) {
		debug("Maxon failed to reach level1");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezClosed);
	if (!success) {
		debug("Kinez se nije zatvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	/*TODO - Sklanjanje ruke */
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(3000));
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position4);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	loaded = true;
	return true;
}

bool ArmExecutor::ReleaseObjectFunction(){
	bool success;
	if (!loaded) {
		debug("There's nothing to release");
		return true;
	}
	/*TODO - Namestanje ruke */
	boost::this_thread::sleep(boost::posix_time::milliseconds(300));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonLevel0);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = AX2.rotateToPosition(value.ArmConfigs.AX2Front);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezOpened);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	success = Maxon.rotateMaxon(value.ArmConfigs.MaxonTop);
	if (!success) {
		debug("Maxon failed to reach level0");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
	success = Kinez.rotateToPosition(value.ArmConfigs.KinezClosed);
	if (!success) {
		debug("Kinez se nije zatvorio");
		return false;
	}
	success = AX1.rotateToPosition(value.ArmConfigs.AX1Position4);
	if (!success) {
		debug("Kinez se nije otvorio");
		return false;
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
	loaded = false;
	return true;
}

}
