#include "FlapExecutor.h"

namespace executor {

string FlapExecutor::NAME = "FlapExecutor";

void FlapExecutor::suscribe(){
    this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&FlapExecutor::processActuatorCommand));
    // dodati registrovanje get state commande // TODO
}

void FlapExecutor::mapping(){

    pipeLeft.setServoSlaveAddress(char(1));
    pipeLeft.setServoPositionAddress(char(2));
    pipeLeft.setServoStatusSetAddress(char(14));
    pipeLeft.setServoStatusReadAddress(char(15));

    pipeRight.setCoilSlaveAddress(char(1));
    pipeRight.setCoilAddress(char(3));
    //pipeRight.setServoStatusSetAddress(char(16));
    //pipeRight.setServoStatusReadAddress(char(17));

    reload(&value, executorName);

    //pipeLeft.rotateToPosition(value.FlapConfigs.flapLeft.close);
    //pipeRight.rotateToPosition(value.FlapConfigs.flapRight.close);

}

bool FlapExecutor::KickRightFunction(){
    debug("KickRight");
    bool success;

    //success = pipeRight.rotateToPosition(value.FlapConfigs.flapRight.open);
    success = pipeRight.setCoilState(false);

    return success;
}

bool FlapExecutor::KickLeftFunction(){
    debug("KickLeft");
    bool success;

    success = pipeLeft.rotateToPosition(value.FlapConfigs.flapLeft.open);

    return success;
}

bool FlapExecutor::UnKickRightFunction(){
    debug("UnKickRight");
    bool success;

    //success = pipeRight.rotateToPosition(value.FlapConfigs.flapRight.close);
    success = pipeRight.setCoilState(true);

    return success;
}

bool FlapExecutor::UnKickLeftFunction(){
    debug("UnKickLeft");
    bool success;

    //success = pipeLeft.rotateToPosition(value.FlapConfigs.flapLeft.close);
    pipeLeft.setServoSpeed(10);

    return success;
}

} // end namespace
