#include "miniPumpExecutor.h"


namespace executor {

	string MiniPumpExecutor::NAME = "MiniPumpExecutor";

	void MiniPumpExecutor::suscribe(){
		this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&MiniPumpExecutor::processActuatorCommand));
	}

	void MiniPumpExecutor::mapping() {
		FrontPump.setCoilSlaveAddress(1);
		FrontPump.setCoilAddress(8);

		BackPump.setCoilSlaveAddress(1);
		BackPump.setCoilAddress(7);

		reload(&value,executorName);
	}

	bool MiniPumpExecutor::TurnOnFrontPumpFunction() {
		debug("Front Pump - TurnOn");
		return FrontPump.setCoilState(false);	// Kontra logika
	}

	bool MiniPumpExecutor::TurnOffFrontPumpFunction() {
		debug("Front Pump - TurnOff");
		return FrontPump.setCoilState(true); // Kontra logika
	}


	bool MiniPumpExecutor::TurnOnBackPumpFunction() {
		debug("Back pump - TurnOn");
		return BackPump.setCoilState(false); // Kontra logika
	}

	bool MiniPumpExecutor::TurnOffBackPumpFunction() {
		debug("Back pump - Turnoff");
		return BackPump.setCoilState(true); // Kontra logika
	}

}
