#ifndef _MINI_PUMP_EXECUTOR_H
#define _MINI_PUMP_EXECUTOR_H

#include "executors/ExecutorCommon.h"


namespace executor {

class MiniPumpExecutor: public ExecutorCommon {
public:
    MiniPumpExecutor():ExecutorCommon(this->NAME){}
    static string NAME;
    void suscribe();
    void mapping();
private:
    bool TurnOnFrontPumpFunction();
    bool TurnOffFrontPumpFunction();
    bool TurnOnBackPumpFunction();
    bool TurnOffBackPumpFunction();
    
    ElectroSwitch FrontPump;
    ElectroSwitch BackPump;

};


}

#endif	// _MINI_PUMP_EXECUTOR_H
