#include "pumpExecutor.h"


namespace executor {

string PumpExecutor::NAME = "PumpExecutor";

void PumpExecutor::suscribe(){
	this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&PumpExecutor::processActuatorCommand));
}

void PumpExecutor::mapping() {
	LeftPump.setCoilSlaveAddress(1);
	LeftPump.setCoilAddress(22);

	RightPump.setCoilSlaveAddress(1);
	RightPump.setCoilAddress(25);

	MidPump.setCoilSlaveAddress(1);
	MidPump.setCoilAddress(20);

	Panel.setCoilSlaveAddress(1);
	Panel.setCoilAddress(17);

	TopPump.setCoilSlaveAddress(1);
	TopPump.setCoilAddress(24);

	LeftSensor.setConfig(1,6,50,this,true);
	LeftSensor.RegisterSensor();
	
	MidSensor.setConfig(1,6,51,this,true);
	MidSensor.RegisterSensor();

	RightSensor.setConfig(1,6,52,this,true);
	RightSensor.RegisterSensor();

	reload(&value,executorName);
}

bool PumpExecutor::TurnOnLeftPumpFunction() {
    bool success;
    debug("Left pump - TurnOn");
    LeftSensor.StartSensor();
    success = LeftPump.setCoilState(false); // Kontra logika
    boost::this_thread::sleep(boost::posix_time::milliseconds(value.PumpConfigs.pumpSensorCheckDelay));
    if (LeftSensor.ReadSensor()){
	    debug("Left pump caught something");
    }
    else
    {
	    debug("Left pump caught nothing");
	    TurnOffLeftPumpFunction();
    }
    return success;
}

bool PumpExecutor::TurnOffLeftPumpFunction() {
    bool success;
    debug("Left pump - Turnoff");
    LeftSensor.StopSensor();
    success = LeftPump.setCoilState(true); // Kontra logika
    return success;
}


bool PumpExecutor::TurnOnRightPumpFunction() {
    bool success;
    debug("Right pump - TurnOn");
    RightSensor.StartSensor();
    success = RightPump.setCoilState(false); // Kontra logika
    boost::this_thread::sleep(boost::posix_time::milliseconds(value.PumpConfigs.pumpSensorCheckDelay));
    if (RightSensor.ReadSensor()){
	    debug("Right pump caught something");
    }
    else
    {
	    debug("Right pump caught nothing");
	    TurnOffRightPumpFunction();
    }
    return success;
}

bool PumpExecutor::TurnOffRightPumpFunction() {
    bool success;
    debug("Right pump - Turnoff");
    RightSensor.StopSensor();
    success = RightPump.setCoilState(true); // Kontra logika
    return success;
}

bool PumpExecutor::TurnOnMidPumpFunction() {
    bool success;
    debug("Mid pump - TurnOn");
    MidSensor.StartSensor();
    success = MidPump.setCoilState(false); // Kontra logika
    boost::this_thread::sleep(boost::posix_time::milliseconds(value.PumpConfigs.pumpSensorCheckDelay));
    if (MidSensor.ReadSensor()){
	    debug("Mid pump caught something");
    }
    else
    {
	    debug("Mid pump caught nothing");
	    TurnOffMidPumpFunction();
    }
    return success;
}

bool PumpExecutor::TurnOffMidPumpFunction() {
    bool success;
    debug("Mid pump - TurnOn");
    MidSensor.StopSensor();
    success = MidPump.setCoilState(true); // Kontra logika
    return success;
}

bool PumpExecutor::TurnOffTopPumpFunction() {
    bool success;
    debug("Top pump - TurnOn");
    success = TopPump.setCoilState(true); // Kontra logika
    return success;
}

bool PumpExecutor::TurnOnTopPumpFunction() {
    bool success;
    debug("Top pump - TurnOn");
    success = TopPump.setCoilState(true); // Kontra logika
    return success;
}
bool PumpExecutor::ExtendPanelFunction() {
    debug("Panel - open");
    return Panel.setCoilState(false);
}

bool PumpExecutor::RetractPanelFunction() {
    debug("Panel - close");
    return Panel.setCoilState(true);
}
}
