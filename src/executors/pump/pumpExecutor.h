#ifndef _PUMP_EXECUTOR_H
#define _PUMP_EXECUTOR_H

#include "executors/ExecutorCommon.h"

using namespace sensor;
using namespace modbus;

namespace executor {

class PumpExecutor: public ExecutorCommon, public SensorDriverInterface {
public:
    PumpExecutor():ExecutorCommon(this->NAME),SensorDriverInterface(){}
    static string NAME;
    void suscribe();
    void mapping();
private:
    bool TurnOnLeftPumpFunction();
    bool TurnOffLeftPumpFunction();
    bool TurnOnRightPumpFunction();
    bool TurnOffRightPumpFunction();
    bool TurnOnMidPumpFunction();
    bool TurnOffMidPumpFunction();
    bool TurnOnTopPumpFunction();
    bool TurnOffTopPumpFunction();
    bool ExtendPanelFunction();
    bool RetractPanelFunction();
    
    ElectroSwitch LeftPump; 
    ElectroSwitch RightPump;
    ElectroSwitch MidPump;
    ElectroSwitch TopPump;
    ElectroSwitch Panel;

    SensorDriver LeftSensor;
    SensorDriver MidSensor;
    SensorDriver RightSensor;

};


}

#endif	// _PUMP_EXECUTOR_H
