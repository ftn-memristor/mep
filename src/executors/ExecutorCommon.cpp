#include "ExecutorCommon.h"

namespace executor{

void ExecutorCommon::init(){
    actuatorHandles[ActuatorType::RELOAD_CONFIG]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::reloadConfig);
    actuatorHandles[ActuatorType::GET_POPCORN]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getPopcorn);
    actuatorHandles[ActuatorType::UNLOAD_POPCORN]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::unloadPopcorn);
    actuatorHandles[ActuatorType::UMBRELLA_OPEN]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::umbrellaOpen);
    actuatorHandles[ActuatorType::UMBRELLA_CLOSE]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::umbrellaClose);
    actuatorHandles[ActuatorType::PUMP_LEFT_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffLeftPump);
    actuatorHandles[ActuatorType::PUMP_LEFT_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnLeftPump);
    actuatorHandles[ActuatorType::PUMP_RIGHT_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffRightPump);
    actuatorHandles[ActuatorType::PUMP_RIGHT_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnRightPump);
    actuatorHandles[ActuatorType::PUMP_MID_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffMidPump);
    actuatorHandles[ActuatorType::PUMP_MID_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnMidPump);
    actuatorHandles[ActuatorType::PUMP_TOP_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnTopPump);
    actuatorHandles[ActuatorType::PUMP_TOP_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffTopPump);
    actuatorHandles[ActuatorType::PUMP_PANEL_IN]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::retractPanel);
    actuatorHandles[ActuatorType::PUMP_PANEL_OUT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::extendPanel);
    actuatorHandles[ActuatorType::MAGNET_LOW]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::lowerMagnet);
    actuatorHandles[ActuatorType::MAGNET_HIGH]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::raiseMagnet);
    actuatorHandles[ActuatorType::ARM_GET_L0]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getObjectL0);
    actuatorHandles[ActuatorType::ARM_GET_L1]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getObjectL1);
    actuatorHandles[ActuatorType::ARM_GET_L2]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getObjectL2);
    actuatorHandles[ActuatorType::ARM_RELEASE]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::releaseObject);
    actuatorHandles[ActuatorType::ARM_OUT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::takeArmOut);
    actuatorHandles[ActuatorType::PUMP_FRONT_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnFrontPump);
    actuatorHandles[ActuatorType::PUMP_FRONT_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffFrontPump);
    actuatorHandles[ActuatorType::PUMP_BACK_ON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOnBackPump);
    actuatorHandles[ActuatorType::PUMP_BACK_OFF]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::turnOffBackPump);
    actuatorHandles[ActuatorType::KICK_RIGHT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::kickRight);
    actuatorHandles[ActuatorType::KICK_LEFT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::kickLeft);
    actuatorHandles[ActuatorType::UNKICK_RIGHT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::unKickRight);
    actuatorHandles[ActuatorType::UNKICK_LEFT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::unKickLeft);
    actuatorHandles[ActuatorType::GET_OBJECT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getObject);
    actuatorHandles[ActuatorType::UNLOAD_OBJECT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::unloadObject);
    actuatorHandles[ActuatorType::RELOAD_CONFIG]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::reloadConfig);
    actuatorHandles[ActuatorType::GET_OBJECT_STOP]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::getObjectStop);
    actuatorHandles[ActuatorType::START_BRXON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::startBrxon);
    actuatorHandles[ActuatorType::STOP_BRXON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::stopBrxon);
    actuatorHandles[ActuatorType::START_BEACON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::startBeacon);
    actuatorHandles[ActuatorType::STOP_BEACON]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::stopBeacon);
    actuatorHandles[ActuatorType::LEAVE_CARPET]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::leaveCarpet);
    actuatorHandles[ActuatorType::CARPET_LEAVE]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::carpetLeave);
    actuatorHandles[ActuatorType::CARPET_POSITION_1]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::carpetPosition1);
    actuatorHandles[ActuatorType::CARPET_POSITION_2]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::carpetPosition2);
    actuatorHandles[ActuatorType::CARPET_POSITION_OPEN]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::carpetPositionOpen);
    actuatorHandles[ActuatorType::CARPET_POSITION_CLOSE]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::carpetPositionClose);
    actuatorHandles[ActuatorType::CALLBACK_GET_LEFT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::callbackGetLeft);
    actuatorHandles[ActuatorType::CALLBACK_GET_RIGHT]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::callbackGetRight);
    actuatorHandles[ActuatorType::START_DETECTION]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::startDetection);
    actuatorHandles[ActuatorType::STOP_DETECTION]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::stopDetection);

    //enemy detecotr
    actuatorHandles[ActuatorType::SENSOR_CALLBACK]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::sensorCommand);
    actuatorHandles[ActuatorType::BRKON_CALLBACK]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::brkonCommand);
    actuatorHandles[ActuatorType::BEACON_MALI_CALLBACK]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::beaconMaliCommand);
    actuatorHandles[ActuatorType::BEACON_VELIKI_CALLBACK]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::beaconVelikiCommand);




    actuatorHandles[ActuatorType::UNIVERSAL_SET_SWITCH]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::universalSetSwitch);
    actuatorHandles[ActuatorType::UNIVERSAL_SET_SERVO]=static_cast<ActuatorCommandHandle>(&ExecutorCommon::universalSetServo);

    suscribeToSensore();
    suscribe();
    mapping();

}




void ExecutorCommon::processActuatorCommand(Command *_command){
    commandQueueLock.lock();
    commandsToProcess.push(Instruction(_command));
    commandQueueLock.unlock();
    queueNotEmpty.notify_one();
}

ExecutorCommon::Instruction ExecutorCommon::getNextCommand(){  // if there is more then one command = send error to old one onda execute new one , returns new command
    unique_lock<boost::mutex> lock(commandQueueLock);
    ActuatorCommand* newCommand = NULL;

    //while (instructionQueue.empty()) {
    //    queueNotEmpty.wait(lock);
    //}

    while (commandsToProcess.empty()) {
        queueNotEmpty.wait(lock);
    }

    while(commandsToProcess.size() > 1){
        Instruction cmd = commandsToProcess.front();
        commandsToProcess.pop();
        debug("newer Command, seding error to old");
        ActuatorCommand* cmdAct = cmd.command;
        if(cmd.command->getId() != -1){
            sendResponseFromCommand(cmdAct, ERROR);
        }
    }
    Instruction newInst = commandsToProcess.front();
    commandsToProcess.pop();
    //newCommand = newInst.command;

    return newInst;
}

void ExecutorCommon::main(){
    shouldStop = false;
    while(!shouldStop){

        Instruction inst = getNextCommand();

        if(inst.type == Instruction::STOP){
            return;
        }

        ActuatorCommand* newCommand = (ActuatorCommand*) inst.command;
        if (newCommand!= NULL && currentActuatorCommand!= NULL){
            //send error to old command
            debug("Newer command recived seding error to old one");
            sendResponseFromCommand(currentActuatorCommand, ERROR);
        }

        if (newCommand!=NULL){
            debug("executing new command");
            (this->*actuatorHandles[newCommand->getActuatorType()])(newCommand); // do new command ( map static cast )

        }
    }
}

void ExecutorCommon::stop(){
    commandQueueLock.lock();
    commandsToProcess.push(Instruction::STOP);
    commandQueueLock.unlock();
    queueNotEmpty.notify_one();
}




void ExecutorCommon::mapping(){
    debug("TODO: must redefine mapping");
}

void ExecutorCommon::suscribe(){
    debug("TODO: must redefine suscribe");
}

void ExecutorCommon::reloadConfig(ActuatorCommand* _command){
    debug("reload");
    bool success = false ;
    ReloadConfig* command = (ReloadConfig*) _command;
    currentActuatorCommand = command;
    debug("reload config with exec name: ");
    success = reload(&value, executorName);
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }

}

void ExecutorCommon::getPopcorn(ActuatorCommand* _command){
    bool success = false ;
    GetPopcorn* command = (GetPopcorn*) _command;
    currentActuatorCommand = command ;
    success = GetPopcornFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::unloadPopcorn(ActuatorCommand* _command){
    bool success = false ;
    UnloadPopcorn* command = (UnloadPopcorn*) _command;
    currentActuatorCommand = command;
    success = UnloadPopcornFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::umbrellaOpen(ActuatorCommand* _command){
    bool success = false ;
    UmbrellaOpen* command = (UmbrellaOpen*) _command;
    currentActuatorCommand = command ;
    success = UmbrellaOpenFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::umbrellaClose(ActuatorCommand* _command){
    bool success = false ;
    UmbrellaClose* command = (UmbrellaClose*) _command;
    currentActuatorCommand = command ;
    success = UmbrellaCloseFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::turnOnLeftPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOnLeftPump* command = (TurnOnLeftPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnLeftPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffLeftPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffLeftPump* command = (TurnOffLeftPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffLeftPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOnRightPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOnRightPump* command = (TurnOnRightPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnRightPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffRightPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffRightPump* command = (TurnOffRightPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffRightPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::turnOnMidPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOnMidPump* command = (TurnOnMidPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnMidPumpFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffMidPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffMidPump* command = (TurnOffMidPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffMidPumpFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::turnOnTopPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOnTopPump* command = (TurnOnTopPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnTopPumpFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffTopPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffTopPump* command = (TurnOffTopPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffTopPumpFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::retractPanel(ActuatorCommand* _command){
    bool success = false;
    RetractPanel* command = (RetractPanel*) _command;
    currentActuatorCommand = command ;
    success = RetractPanelFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::extendPanel(ActuatorCommand* _command){
    bool success = false;
    ExtendPanel* command = (ExtendPanel*) _command;
    currentActuatorCommand = command ;
    success = ExtendPanelFunction();
    if (success){
	    sendResponseFromCommand(currentActuatorCommand, SUCCESS);
	    currentActuatorCommand = NULL;
    }else{
	    sendResponseFromCommand(currentActuatorCommand, ERROR);
	    currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::turnOnBackPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOnBackPump* command = (TurnOnBackPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnBackPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffBackPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffBackPump* command = (TurnOffBackPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffBackPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOnFrontPump(ActuatorCommand* _command){
    bool success = false ;
    debug("turnOnFrontPump called");
    TurnOnFrontPump* command = (TurnOnFrontPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOnFrontPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}
void ExecutorCommon::turnOffFrontPump(ActuatorCommand* _command){
    bool success = false ;
    TurnOffFrontPump* command = (TurnOffFrontPump*) _command;
    currentActuatorCommand = command ;
    success = TurnOffFrontPumpFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::lowerMagnet(ActuatorCommand* _command){
	bool success = false ;
	LowerMagnet* command = (LowerMagnet*) _command;
	currentActuatorCommand = command;
	success = LowerMagnetFunction();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}
void ExecutorCommon::raiseMagnet(ActuatorCommand* _command){
	bool success = false ;
	RaiseMagnet* command = (RaiseMagnet*) _command;
	currentActuatorCommand = command;
	success = RaiseMagnetFunction();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}
void ExecutorCommon::getObjectL0Hold(ActuatorCommand* _command){
	bool success = false ;
	GetObjectL0Hold* command = (GetObjectL0Hold*) _command;
	currentActuatorCommand = command;
	success = GetObjectL0HoldFunction();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}

void ExecutorCommon::getObjectL0(ActuatorCommand* _command){
	bool success = false ;
	GetObjectL0* command = (GetObjectL0*) _command;
	currentActuatorCommand = command;
	success = GetObjectL0Function();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}

void ExecutorCommon::getObjectL1(ActuatorCommand* _command){
	bool success = false ;
	GetObjectL1* command = (GetObjectL1*) _command;
	currentActuatorCommand = command;
	success = GetObjectL1Function();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}

void ExecutorCommon::getObjectL2(ActuatorCommand* _command){
	bool success = false ;
	GetObjectL2* command = (GetObjectL2*) _command;
	currentActuatorCommand = command;
	success = GetObjectL2Function();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}

void ExecutorCommon::takeArmOut(ActuatorCommand* _command){
	bool success = false ;
	TakeArmOut* command = (TakeArmOut*) _command;
	currentActuatorCommand = command;
	success = TakeArmOutFunction();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}

void ExecutorCommon::releaseObject(ActuatorCommand* _command){
	bool success = false ;
	ReleaseObject* command = (ReleaseObject*) _command;
	currentActuatorCommand = command;
	success = ReleaseObjectFunction();
	if (success){
		sendResponseFromCommand(currentActuatorCommand, SUCCESS);
		currentActuatorCommand = NULL;
	}else{
		sendResponseFromCommand(currentActuatorCommand, ERROR);
		currentActuatorCommand = NULL;
	}
}


void ExecutorCommon::kickRight(ActuatorCommand* _command){
    bool success = false;
    KickRight* command = (KickRight*) _command;
    currentActuatorCommand = command;
    success = KickRightFunction();
    if (success){
        debug("sendin success");
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        debug("sending error");
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::kickLeft(ActuatorCommand* _command){
    bool success = false;
    KickLeft* command = (KickLeft*) _command;
    currentActuatorCommand = command;
    success = KickLeftFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::unKickRight(ActuatorCommand* _command){
    bool success = false;
    UnKickRight* command = (UnKickRight*) _command;
    currentActuatorCommand = command;
    success = UnKickRightFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::unKickLeft(ActuatorCommand* _command){
    bool success = false;
    UnKickLeft* command = (UnKickLeft*) _command;
    currentActuatorCommand = command;
    success = UnKickLeftFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}


void ExecutorCommon::getObject(ActuatorCommand * _command){
    bool success;
    GetObject* command = (GetObject*) _command;
    currentActuatorCommand = command;
    success =  GetObjectFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::unloadObject(ActuatorCommand* _command){
    bool success;
    UnloadObject *command  = (UnloadObject*) _command;
    currentActuatorCommand = command;
    success = UnloadObjectFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::getObjectStop(ActuatorCommand * _command){
    bool success;
    GetObjectStop* command = (GetObjectStop*) _command;
    currentActuatorCommand = command;
    success =  GetObjectStopFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::startBrxon(ActuatorCommand * _command){
    bool success;
    StartBrxon* command = (StartBrxon*) _command;
    currentActuatorCommand = command;
    success =  StartBrxonFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::startBeacon(ActuatorCommand * _command){
    bool success;
    StartBeacon* command = (StartBeacon*) _command;
    currentActuatorCommand = command;
    success =  StartBeaconFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}


void ExecutorCommon::stopBrxon(ActuatorCommand * _command){
    bool success;
    StopBrxon* command = (StopBrxon*) _command;
    currentActuatorCommand = command;
    success =  StopBrxonFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}


void ExecutorCommon::stopBeacon(ActuatorCommand * _command){
    bool success;
    StopBeacon* command = (StopBeacon*) _command;
    currentActuatorCommand = command;
    success =  StopBeaconFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::leaveCarpet(ActuatorCommand * _command){
    bool success;
    LeaveCarpet* command = (LeaveCarpet*) _command;
    currentActuatorCommand = command;
    success =  LeaveCarpetFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::callbackGetRight(ActuatorCommand * _command){
    bool success;
    CallbackGetRight* command = (CallbackGetRight*) _command;
    currentActuatorCommand = command;
    success =  CallbackGetRightFunction();
    if (success){
        //sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        //sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::callbackGetLeft(ActuatorCommand * _command){
    bool success;
    CallbackGetLeft* command = (CallbackGetLeft*) _command;
    currentActuatorCommand = command;
    success =  CallbackGetLeftFunction();
    if (success){
        //sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        //sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::startDetection(ActuatorCommand * _command){
    bool success;
    StartDetection* command = (StartDetection*) _command;
    currentActuatorCommand = command;
    success =  StartDetectionFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::stopDetection(ActuatorCommand * _command){
    bool success;
    StopDetection* command = (StopDetection*) _command;
    currentActuatorCommand = command;
    success =  StopDetectionFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::carpetLeave(ActuatorCommand * _command){
    bool success;
    CarpetLeave* command = (CarpetLeave*) _command;
    currentActuatorCommand = command;
    success =  CarpetLeaveFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::carpetPosition1(ActuatorCommand * _command){
    bool success;
    CarpetPosition1* command = (CarpetPosition1*) _command;
    currentActuatorCommand = command;
    success =  CarpetPosition1Function();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::carpetPosition2(ActuatorCommand * _command){
    bool success;
    CarpetPosition2* command = (CarpetPosition2*) _command;
    currentActuatorCommand = command;
    success =  CarpetPosition2Function();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::carpetPositionOpen(ActuatorCommand * _command){
    bool success;
    CarpetPositionOpen* command = (CarpetPositionOpen*) _command;
    currentActuatorCommand = command;
    success =  CarpetPositionOpenFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::carpetPositionClose(ActuatorCommand * _command){
    bool success;
    CarpetPositionClose* command = (CarpetPositionClose*) _command;
    currentActuatorCommand = command;
    success =  CarpetPositionCloseFunction();
    if (success){
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    }else{
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::sensorCommand(ActuatorCommand *_command){
    bool success;
    int id;
    bool detected;
    SensorCommand* command = (SensorCommand*) _command;
    currentActuatorCommand = command;
    id = command->getId();
    detected = command->getDetected();
    SensorCallbackFunction(id, detected);
}

void ExecutorCommon::beaconMaliCommand(ActuatorCommand *_command){
    short x;
    short y;
    bool working;
    BeaconMaliCommand* command = (BeaconMaliCommand*) _command;
    currentActuatorCommand = command;
    x = command->getCordX();
    y = command->getCordY();
    working = command->getStatus();
    BeaconMaliCallbackFunction(x,y,working);
}

void ExecutorCommon::beaconVelikiCommand(ActuatorCommand *_command){
    short x;
    short y;
    bool working;
    BeaconVelikiCommand* command = (BeaconVelikiCommand*) _command;
    currentActuatorCommand = command;
    x = command->getCordX();
    y = command->getCordY();
    working = command->getStatus();
    BeaconVelikiCallbackFunction(x,y,working);
}

void ExecutorCommon::brkonCommand(ActuatorCommand *_command){
    short front;
    short back;
    bool detected;
    BrkonCommand* command = (BrkonCommand*) _command;
    front = command->getAngleFront();
    back = command->getAngleBack();
    detected = command->getDetected();
    BrkonCallbackFunction(front,back,detected);
}


void ExecutorCommon::universalSetSwitch(ActuatorCommand *_command) {
    bool success;
    UniversalSetSwitch* command = (UniversalSetSwitch*) _command;
    currentActuatorCommand = command;
    success =  UniversalSetSwitchFunction();
    if (success) {
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    } else {
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}

void ExecutorCommon::universalSetServo(ActuatorCommand *_command) {
    bool success;
    UniversalSetServo* command = (UniversalSetServo*) _command;
    currentActuatorCommand = command;
    success =  UniversalSetServoFunction();
    if (success) {
        sendResponseFromCommand(currentActuatorCommand, SUCCESS);
        currentActuatorCommand = NULL;
    } else {
        sendResponseFromCommand(currentActuatorCommand, ERROR);
        currentActuatorCommand = NULL;
    }
}


bool ExecutorCommon::KickRightFunction(){
    debug("KICK RIGHT: REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::KickLeftFunction(){
    debug("KICK LEFT: REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::UnKickRightFunction(){
    debug("UNKICK RIGHT: REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::UnKickLeftFunction(){
    debug("UNKICK LEFT: REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::GetPopcornFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::UmbrellaOpenFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::UmbrellaCloseFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::UnloadPopcornFunction(){
    debug("REDEFINE PLEASE");
    return false;
}
bool ExecutorCommon::UnloadObjectFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::GetObjectFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::SetSpeedFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::SetPositionFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::GetObjectStopFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::StopBrxonFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::StartBrxonFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::StartBeaconFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::StopBeaconFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::LeaveCarpetFunction(){
    debug("REDEFINE PLEASE");
    return false;
}

bool ExecutorCommon::TurnOnLeftPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffLeftPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}

bool ExecutorCommon::TurnOnRightPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffRightPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOnMidPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffMidPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOnTopPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffTopPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::RetractPanelFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::ExtendPanelFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::LowerMagnetFunction(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}
bool ExecutorCommon::RaiseMagnetFunction(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

bool ExecutorCommon::GetObjectL0Function(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}
bool ExecutorCommon::GetObjectL0HoldFunction(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

bool ExecutorCommon::GetObjectL1Function(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

bool ExecutorCommon::GetObjectL2Function(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

bool ExecutorCommon::ReleaseObjectFunction(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

bool ExecutorCommon::TurnOnFrontPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffFrontPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}

bool ExecutorCommon::TurnOnBackPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TurnOffBackPumpFunction(){
	debug("REDEFINE PLEASE!!!!");
	return false;
}
bool ExecutorCommon::TakeArmOutFunction(){
	debug("REDEFINE PLEASE!!!!!");
	return false;
}

}
