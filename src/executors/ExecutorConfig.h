#ifndef _RELOADEXECUTORCONFIG_H
#define _RELOADEXECUTORCONFIG_H

//#include "AbstractLiftExecutor.h"

#include "boost/algorithm/string.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/foreach.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using std::string;
using std::cout;
using std::endl;
using boost::property_tree::ptree;
using namespace boost::property_tree::xml_parser;

namespace executor{

class ExecutorConfig{

private:
    struct LieftLevelsConfig{
        short level0; // kada smo tek iznad podloge da nosimo sve sto imamo
        short level1;
        short level2;
        short levelBall;
    };

    struct OpenCloseConfig{
        short open;
        short close;
    };

    struct HandConfig{
        short grab;
        short relese;
    };

    struct DoorConfig{
        short openGetObject;
        short openLeaveObejct;
        short close;
    };

    struct TimeConfig{
        int interval;
        int adjust_position;
        int some_time;
    };

    struct TimeConfigLift{
        int liftUp;
        int liftDown;
        int doorOpenClose;
        int handOpenClose;
    };

    struct TimeConfigPopcorn{
        int getPopcorn;
        int unloadPopcorn;
    };
    struct CarpetArmConfig{
        short positionClose;
        short positionOpen;
        short position1;
        short position2;
    };

    ptree pt;


protected:
    struct LiftExecutorConfig {
        LieftLevelsConfig lift;
        DoorConfig door;
        HandConfig hand;
        TimeConfigLift time;
    };

    struct PopcornExecutorConfigs{
        OpenCloseConfig pipeRight;
        OpenCloseConfig backDoor;
        OpenCloseConfig pipeLeft;
        TimeConfigPopcorn time;
    };

    struct FlapExecutorConfigs{
        OpenCloseConfig flapRight;
        OpenCloseConfig flapLeft;
    };

    struct CarpetExecutorConfigs{
        CarpetArmConfig armLeft;
        CarpetArmConfig armRight;
        int armOpenCloseTime;
        int leaveCarpetTime;
    };

    struct PumpExecutorConfigs{
	int pumpSensorCheckDelay;
    };

    struct MagnetExecutorConfigs{
	int magnetUpPosition;
	int magnetDownPosition;
	int magnetUpSpeed;
	int magnetDownSpeed;
    };

    struct ArmExecutorConfigs{
	int AX1Closed;
	int AX1Position1;
	int AX1Position2;
	int AX1Position3;
	int AX1Position4;
	int AX1Position5;
	int AX1Front;
	int KinezClosedHold;
	int KinezClosed;
	int KinezOpened;
	int KinezOpenedHold;
	int AX2Closed;
	int AX2Position1;
	int AX2Position2;
	int AX2Position3;
	int AX2Position4;
	int AX2Position5;
	int AX2Front;
	int MaxonLevel0;
	int MaxonLevel1;
	int MaxonLevel2;
	int MaxonTop;
    };
    struct ConfigValues{   // TODO UNION
        LiftExecutorConfig LiftConfigs;
        PopcornExecutorConfigs PopcornConfigs;
	PumpExecutorConfigs PumpConfigs;
	MagnetExecutorConfigs MagnetConfigs;
	ArmExecutorConfigs ArmConfigs;
        FlapExecutorConfigs FlapConfigs;
        CarpetExecutorConfigs CarpetConfig;
    };


public:
    ExecutorConfig();
    virtual bool reloadF();
    bool reload(ConfigValues* values, string _executorName);
};

}


#endif // _RELOADEXECUTORCONFIG_H
