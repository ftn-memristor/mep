#ifndef HardExecutor_h
#define HardExecutor_h

#include "executors/ExecutorCommon.h"
#include "executors/msg/ParamsStore.h"
#include <vector>

namespace executor {
class UniversalExecutor : public ExecutorCommon {
public:
        UniversalExecutor() : ExecutorCommon(this->NAME){
        }
        static string NAME;
        void suscribe();
        void mapping();
private:
        ElectroSwitch electroSwitch;
        ServoDriver servoDriver;

        bool UniversalSetSwitchFunction();
        bool UniversalSetServoFunction();
        bool UniversalWriteFunction();
};
}

#endif  // HardExecutor_h
