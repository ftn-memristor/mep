#include "UniversalExecutor.h"

namespace executor {

string UniversalExecutor::NAME = "UniversalExecutor";

void UniversalExecutor::suscribe() {
        this->registerCommand(ActuatorCommand::NAME,
          static_cast<commandCallback>(&UniversalExecutor::processActuatorCommand));
}


void UniversalExecutor::mapping() {
  electroSwitch.setCoilSlaveAddress(1);
  electroSwitch.setCoilAddress(1);

  reload(&value, executorName);
}


bool UniversalExecutor::UniversalSetSwitchFunction() {
  ParamsStore *paramStore = ParamsStore::getInstance();
  std::vector<int> params = paramStore->get(UNIVERSAL_SET_SWITCH);

  int slaveAddress = params[0];
  int address = params[1];
  int state = params[2];

  // Create new object
  electroSwitch.setCoilSlaveAddress(slaveAddress);
  electroSwitch.setCoilAddress(address);

  // Set value
  electroSwitch.setCoilState(state);
}

bool UniversalExecutor::UniversalSetServoFunction() {
  ParamsStore *paramStore = ParamsStore::getInstance();
  std::vector<int> params = paramStore->get(UNIVERSAL_SET_SERVO);

  int slaveAddress = params[0];
  int address = params[1];
  int angle = params[2];

  // Create new object
  servoDriver.setServoSlaveAddress(slaveAddress);
  servoDriver.setServoPositionAddress(address);

  // Set value
  servoDriver.rotateToPosition(angle);
}

bool UniversalExecutor::UniversalWriteFunction() {
  // TODO: Should write direct to Modbus
}

}  // namespace executor
