#ifndef _PNEUMATIKA_EXECUTOR
#define _PNEUMATIKA_EXECUTOR 

#include "executors/ExecutorCommon.h"
#include "drivers/actuators/Pneumatika.h"

using namespace pneumatika;

namespace executor {
	class PneumatikaExecutor: public ExecutorCommon {
		public:
			PneumatikaExecutor():ExecutorCommon(this->NAME){}
			static string NAME;
		private:
			void mapping(); // Mozda treba da bude public
			//void suscribe(); // Mozda treba da bude public
			bool Up();
			bool Down();
			bool In();
			bool Out();
			Pneumatika horizontala;
			Pneumatika vertikala;
	};
}
#endif /* ifndef _PNEUMATIKA_EXECUTOR */
