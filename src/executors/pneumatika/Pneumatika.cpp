#include "Pneumatika.h"

namespace executor {
	string PneumatikaExecutor::NAME="PneumatikaTest";
//	void PneumatikaExecutor::suscribe(){
//		this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&PneumatikaExecutor::processActuatorCommand));
//		lastState.Aveable = true;
//		lastState.Quantity = 0;
//		executorName = this->NAME;
//	}
	void PneumatikaExecutor::mapping() {
		horizontala.setPneumatikaSlaveAddress(char(3));
		horizontala.setPneumatikaAddress(char(4));
		vertikala.setPneumatikaSlaveAddress(char(5));
		vertikala.setPneumatikaAddress(char(6));
	}
	
	bool PneumatikaExecutor::Up() {
		return vertikala.setPosition(1);
	}
	bool PneumatikaExecutor::Down() {
		return vertikala.setPosition(0);
	}
	bool PneumatikaExecutor::In() {
		return horizontala.setPosition(0);
	}
	bool PneumatikaExecutor::Out() {
		return horizontala.setPosition(1);
	}
}
