/*
 * Created by LukicDarkoo
 *
 * Budz^2
 * Should store params for an executor methods
 */

#ifndef ParamsStore_h
#define ParamsStore_h

#include <map>
#include <vector>

namespace executor {

class ParamsStore {
  public:
    static ParamsStore* getInstance();
    bool set(int methodId, std::string methodWithParams);
    std::vector<int> get(int methodId);

  private:
    // Override default constructor
    ParamsStore();

    static std::vector<int> ParamsStore::getParamByIndex(std::string str);

    static ParamsStore *paramsStore;
    // std::map<int, std::map<char, char>> store;
    // std::map<int, std::map< int, std::map<char, char> >> store;


    // METHOD_ENUM -> Lifo of string parameters
    std::map<int, std::map <int, std::string>> store;
  };

} // namespace executors

#endif
