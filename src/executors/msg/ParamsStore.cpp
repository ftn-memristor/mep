#include "ParamsStore.h"


namespace executor {

ParamsStore* ParamsStore::paramsStore = nullptr;

ParamsStore* ParamsStore::getInstance() {
  if (paramsStore == nullptr) {
    paramsStore = new ParamsStore();
  }
  return paramsStore;
}

std::vector<int> ParamsStore::getParamByIndex(std::string str) {
  std::vector<int> out;

	int lastFoundIndex = 0;
	int previousFoundIndex = 0;
	int currentIndex = 0;

  std::string tempString;

	lastFoundIndex = str.find("|", lastFoundIndex);
	while (lastFoundIndex != std::string::npos) {
		previousFoundIndex = lastFoundIndex;
		lastFoundIndex = str.find("|", lastFoundIndex + 1);


		if (lastFoundIndex != std::string::npos) {
			tempString = str.substr(previousFoundIndex + 1, lastFoundIndex - previousFoundIndex - 1);
      out.insert(out.end(), std::stoi(tempString));
		} else {
			tempString = str.substr(previousFoundIndex + 1);
      out.insert(out.end(), std::stoi(tempString));
		}

		currentIndex++;
	}

	return out;
}

bool ParamsStore::set(int methodId, std::string methodWithParams) {
  int index = store[methodId].end()->first + 1;
  store[methodId][index] = methodWithParams;

  return true;
}

std::vector<int> ParamsStore::get(int methodId) {
  std::string value = store[methodId].begin()->second;
  store[methodId].erase(store[methodId].begin());

  return ParamsStore::getParamByIndex(value);
}

ParamsStore::ParamsStore() { }

} // namespace executor
