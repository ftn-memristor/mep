#ifndef UmbrellaExecutor_h
#define UmbrellaExecutor_h

#include "executors/ExecutorCommon.h"
#include "executors/msg/ParamsStore.h"

namespace executor {
class UmbrellaExecutor : public ExecutorCommon {
public:
        UmbrellaExecutor() : ExecutorCommon(this->NAME){
        }
        static string NAME;
        void suscribe();
        void mapping();
private:
        ElectroSwitch electroSwitch;

        bool UmbrellaOpenFunction();
        bool UmbrellaCloseFunction();
};
}

#endif  // UmbrellaExecutor_h
