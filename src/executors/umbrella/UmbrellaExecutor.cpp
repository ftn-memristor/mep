#include "UmbrellaExecutor.h"

namespace executor {

string UmbrellaExecutor::NAME = "UmbrellaExecutor";

void UmbrellaExecutor::suscribe() {
        this->registerCommand(ActuatorCommand::NAME,
          static_cast<commandCallback>(&UmbrellaExecutor::processActuatorCommand));
}


void UmbrellaExecutor::mapping() {
  electroSwitch.setCoilSlaveAddress(1);
  electroSwitch.setCoilAddress(26);

  reload(&value, executorName);
}


bool UmbrellaExecutor::UmbrellaOpenFunction() {
  electroSwitch.setCoilState(0);
}

bool UmbrellaExecutor::UmbrellaCloseFunction() {
  electroSwitch.setCoilState(1);
}

}  // namespace executor
