#include "switchTestExecutor.h"


namespace executor {

string SwitchTestExecutor::NAME = "SwitchTestExecutor";

void SwitchTestExecutor::suscribe(){
	this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&SwitchTestExecutor::processActuatorCommand));
}

void SwitchTestExecutor::mapping() {
	SwitchTest.setCoilSlaveAddress(1);
	SwitchTest.setCoilAddress(17);

	FrontRightPump.setCoilSlaveAddress(1);
	FrontRightPump.setCoilAddress(24);

	reload(&value,executorName);
}

bool SwitchTestExecutor::TurnOnSwitchFunction() {
    debug("Switch test - TurnOn");
    return SwitchTest.setCoilState(false); // Kontra logika
}

bool SwitchTestExecutor::TurnOffSwitchFunction() {
    debug("Switch test - Turnoff");
    return SwitchTest.setCoilState(true); // Kontra logika
}


bool SwitchTestExecutor::TurnOnFrontRightPumpFunction() {
    debug("Fron right pump - TurnOn");
    return FrontRightPump.setCoilState(false); // Kontra logika
}

bool SwitchTestExecutor::TurnOffFrontRightPumpFunction() {
    debug("Front right pump - Turnoff");
    return FrontRightPump.setCoilState(true); // Kontra logika
}

}
