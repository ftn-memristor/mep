#ifndef _SWITCH_TEST_EXECUTOR_H
#define _SWITCH_TEST_EXECUTOR_H

#include "executors/ExecutorCommon.h"


namespace executor {

class SwitchTestExecutor: public ExecutorCommon {
public:
    SwitchTestExecutor():ExecutorCommon(this->NAME){}
    static string NAME;
    void suscribe();
    void mapping();
private:
    bool TurnOnSwitchFunction();
    bool TurnOffSwitchFunction();
    bool TurnOnFrontRightPumpFunction();
    bool TurnOffFrontRightPumpFunction();
    
    ElectroSwitch SwitchTest; // TODO Change to FrontLeftPump
    ElectroSwitch FrontRightPump;

};


}

#endif	// _SWITCH_TEST_EXECUTOR_H
