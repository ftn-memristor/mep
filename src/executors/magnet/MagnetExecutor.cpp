#include "MagnetExecutor.h"

namespace executor {

string MagnetExecutor::NAME = "MagnetExecutor";

void MagnetExecutor::suscribe(){
	this->registerCommand(ActuatorCommand::NAME, static_cast<commandCallback>(&MagnetExecutor::processActuatorCommand));
}

void MagnetExecutor::mapping() {
	MagnetServo.setServoSlaveAddress(1);
	MagnetServo.setServoSpeedAddress(3);
	MagnetServo.setServoPositionAddress(2);

	reload(&value,executorName);

	MagnetServo.rotateToPosition(value.MagnetConfigs.magnetUpPosition);
	MagnetServo.setServoSpeed(value.MagnetConfigs.magnetDownSpeed);
}

bool MagnetExecutor::LowerMagnetFunction() {
	bool success;
	debug("Magnet - Lower");
	success = MagnetServo.setServoSpeed(value.MagnetConfigs.magnetDownSpeed);
	if(!success){
		debug("Failed to set Magnet speed");
		return false;
	}
	success = MagnetServo.rotateToPosition(value.MagnetConfigs.magnetDownPosition);
	if(!success){
		debug("Failed to move magnet");
		return false;
	}
	return true;
}

bool MagnetExecutor::RaiseMagnetFunction() {
	bool success;
	debug("Magent - Raise");
	success = MagnetServo.setServoSpeed(value.MagnetConfigs.magnetUpSpeed);
	if(!success){
		debug("Failed to set Magnet speed");
		return false;
	}
	success = MagnetServo.rotateToPosition(value.MagnetConfigs.magnetUpPosition);
	if(!success){
		debug("Failed to move magnet");
		return false;
	}
	return success;
}
}
