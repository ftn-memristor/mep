#ifndef _MAGNET_EXECUTOR
#define _MAGNET_EXECUTOR

#include "executors/ExecutorCommon.h"

namespace executor {

class MagnetExecutor: public ExecutorCommon {
public:
	MagnetExecutor():ExecutorCommon(this->NAME){}
	static string NAME;
	void suscribe();
	void mapping();
private:
	bool LowerMagnetFunction();
	bool RaiseMagnetFunction();


	ServoDriver MagnetServo;
};
}

#endif /* ifndef _MAGNET_EXECUTOR */
