# Configure Rasspberry Pi 

## Install WiringPi
'''
	sudo apt-get install git-core
	git clone git://git.drogon.net/wiringPi
	cd wiringPi
	./build
'''
[Reference](http://wiringpi.com/download-and-install/)

## Install Boost
'''
	sudo apt-get install libboost-regex-dev libboost-filesystem-dev libboost-locale-dev libboost-serialization-dev libboost-timer-dev libboost-thread-dev libboost-system-dev libboost-signals-dev
'''

## Enable UART
- Open Raspberry Pi configuration menu `sudo raspbi-config`
- Choose `Advanced Options > Serial`
- Select `No`

## Enable Fast Debugging Transfer
SSH file transfer is slow so we have choosen FTP to send binary to Raspberry Pi.  

```pip install pyftpdlib```

[Reference](http://stackoverflow.com/questions/6811258/one-line-ftp-server)

TODO
