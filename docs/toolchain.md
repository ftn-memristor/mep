# Toolchain
	sudo apt-get install git rsync cmake lib32z1

	git clone git://github.com/raspberrypi/tools.git

	cd tools
	mkdir rootfs
	rsync -rl --delete-after --safe-links pi@192.168.1.247:/{lib,usr} [your eurobot directory]/tools/rootfs

## Reference
http://stackoverflow.com/questions/19162072/installing-raspberry-pi-cross-compiler/19269715#19269715

