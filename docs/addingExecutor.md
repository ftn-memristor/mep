# Adding command
- Change `/src/executors/msg/ActuatorCommand.h`
	- `Executors` (Enumeracija executora)
	- `ActuatorType` (Enumeracija metoda executora)
- Definisati ActuatorAction inside `src/executors/msg/ActuatorCommand.cpp`

# Adding executor configuration
- Edit XML `/Config.xml`
- New structure to `/src/executors/ExecutorConfig.h` and add that structure to `struct ConfigValues`

# Include executor to Robot.cpp
- Include executor
- Init executor class with others...
- execMgr->addExecutor(&executor)

# ExecutorCommon.h
- Define a stub for every one of the executor functions

# ExecutorCommon.cpp
- For each executorCommand add a fuction that calls ExecutorCommandFunction (declared virtual in ExecutorCommon)
- Add ExecutorCommandFunction and declare it as virtual bool returning false. It should be reimplemented in the actual executor .cpp file.
- Add actuatorHandles in the ExecutorCommon::init()

# Pipetask.cpp
- Add actionMap["Action"] = ACTION in PipeTask::initScript()
- Add executor to EXEC_NAME case (line 126)
- Add executor commands to EXEC_OPTION case (line 142)
- sendCommand


